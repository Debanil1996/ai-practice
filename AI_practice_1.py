#!/usr/bin/env python
# coding: utf-8

# In[1]:


a=23


# In[2]:


print(a)


# In[3]:


import keyword

print(keyword.kwlist)

print("\n The number of total number of keywords",len(keyword.kwlist))


# In[4]:


for i in range(10):
    print(i)
    print('Data',i*2)


# In[5]:


a=23
b=45
print(a,b)


# In[6]:


a,b,c=22,98,"DD"


# In[7]:


print(a,b,c);


# In[8]:


a=3
print(id(a));


# In[9]:


a=23
print(id(a))


# In[10]:


a=1+2j
print(a," is a complex number")
print(isinstance(a,complex))


# In[11]:


c=True;
print(c)


# # Python Tuples
# 
# Here doing the checking of tuples .
# 

# In[2]:


t=(1.5,34,23)
print(t[1]);


# In[4]:


t[1]=23


# In[ ]:





# ## Output Formatting
#     -  Place value into the required string

# In[4]:


a=10
b=20
print("I have {} chocolates and {} pie".format(a,b))


# # Arithmetic
# 

# In[6]:


a=24;
b=45;
print(a**b)


# In[15]:


a=10
b=10
j=[1,2,3]
k=[1,2,3]
l="DREF"
m="DREF"
print(a is b,id(a),id(b))
print(j is k)
print(l is m)


# # MemberShip Operators

# In[16]:


lst =[1,2,3,4,5,6]
print(3 in lst)

